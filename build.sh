#!/bin/bash


# 
# comment this out before check in - we will inherit these from .gitabl-ci.yml
#CI_REGISTRY_IMAGE="registry.gitlab.lightcloudit.com/ael/vote-demo-vote"
#CI_BUILD_REF_NAME="jliuTest"
#APP_VERSION="v1.0.2"
#COMMITHASH="0123ddee"
#CI_PROJECT_NAME="vote-demo-vote"
#COMMITCOUNT=$(git rev-list --left-right --count master...$COMMITHASH | awk '{print $NF}')
#

if [ $1 ]; then
  REF_NAME="$1"
else
  REF_NAME="${CI_BUILD_REF_NAME}"
fi

if [ "$REF_NAME" = "master" ]; then
  CONTAINER="$CI_REGISTRY_IMAGE:${APP_VERSION}"
  # looks like rancher now only supports semver syntax.... invalid syntax loading semver for version string develop_v1.0.2_52_1372ee79
  #TAG="${REF_NAME}_${APP_VERSION}_${COMMITCOUNT}_${COMMITHASH}"
  TAG="${APP_VERSION}"
else
  CONTAINER="$CI_REGISTRY_IMAGE:${REF_NAME}_${APP_VERSION}_${COMMITCOUNT}_${COMMITHASH}"
  # looks like rancher now only supports semver syntax.... invalid syntax loading semver for version string develop_v1.0.2_52_1372ee79
  #TAG="${REF_NAME}_${APP_VERSION}_${COMMITCOUNT}_${COMMITHASH}"
  TAG="${APP_VERSION}-${COMMITCOUNT}_${COMMITHASH}"
fi



echo "container $CONTAINER"
echo "tag $TAG"

if [ ${COMMITHASH} = "" ]; then
  exit 1
fi


if [ ! -d templates/$REF_NAME ];
then
  COUNT=0
  mkdir -p templates/$REF_NAME/$COUNT
else
  CURR=$(find templates/$REF_NAME -type d -ls |awk '{print $NF}' |sort | tail -1 | cut -d/ -f3)
  COUNT=$(expr $CURR + 1)
  mkdir -p templates/$REF_NAME/$COUNT
fi

if [ $(/usr/bin/which perl) ];
then
  cp base/catalogIcon.png templates/$REF_NAME/catalogIcon.png
  cp base/config.tmpl templates/$REF_NAME/config.yml
  perl -pi -e "s!\{\{ .Project \}\}!$CI_PROJECT_NAME!g" templates/$REF_NAME/config.yml
  perl -pi -e "s!\{\{ .Branch \}\}!$REF_NAME!g" templates/$REF_NAME/config.yml
  perl -pi -e "s!\{\{ .Tag \}\}!$TAG!g" templates/$REF_NAME/config.yml
  cp base/docker-compose.tmpl templates/$REF_NAME/$COUNT/docker-compose.yml
  perl -pi -e "s!\{\{ .Project \}\}!$CI_PROJECT_NAME!g" templates/$REF_NAME/$COUNT/docker-compose.yml
  perl -pi -e "s!\{\{ .Container \}\}!$CONTAINER!g" templates/$REF_NAME/$COUNT/docker-compose.yml
  cp base/rancher-compose.tmpl templates/$REF_NAME/$COUNT/rancher-compose.yml
  perl -pi -e "s!\{\{ .Project \}\}!$CI_PROJECT_NAME!g" templates/$REF_NAME/$COUNT/rancher-compose.yml
  perl -pi -e "s!\{\{ .Branch \}\}!$REF_NAME!g" templates/$REF_NAME/$COUNT/rancher-compose.yml
  perl -pi -e "s!\{\{ .Tag \}\}!$TAG!g" templates/$REF_NAME/$COUNT/rancher-compose.yml
  perl -pi -e "s!\{\{ .Count \}\}!$COUNT!g" templates/$REF_NAME/$COUNT/rancher-compose.yml
else
  echo "Error: perl not found!"
  exit 1
fi

